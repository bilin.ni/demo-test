# Start
主应用 join-react http://127.0.0.1:5173/            
子应用 rocket-ui http://localhost:8000          
## qiankun
通过访问路由/sub-app 激活子应用   
cd packages/qiankun     
pnpm install        
pnpm run init       
pnpm run start      
## federation
cd packages/federation      
pnpm install        
pnpm run init       
pnpm run start    

# 测试方式
## federation
join-react 是主应用
测哪个远程子应用，在vite.config.ts 的 remotes 字段里添加
```js
'app': { // 暴露的子应用名
    external: 'http://localhost:3000/remoteEntry.js', // 本地服务地址和filename
    format: "var",
}
```
页面中调用
```js
import { inject, cleanup } from 'app/inject';  // 路径是子应用名/加暴露的路径
```
暴露的路径看子应用exposes
```js
exposes: {
    './inject': '项目文件',
},
```