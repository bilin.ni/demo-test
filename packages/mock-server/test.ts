export interface EntitiesRequest {
    page_number?: number;
    page_size?: number;
    pagable: boolean;
  }
  export interface EntitiesResponse {
    total: number;
    page_number: number;
    page_size: number;
    rows: Array<string>;
  }