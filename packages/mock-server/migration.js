// https://github.com/vega/ts-json-schema-generator
// https://transform.tools/typescript-to-json-schema
const tsj = require("ts-json-schema-generator");
const fs = require("fs");
const path = require("path");
function generateSchema(path, filename) {
  const config = {
    path,
    tsconfig: "./tsconfig.json",
    type: "*",
  };
  const output_path = `${filename}.json`;
  console.log(`generateSchema ------------->${output_path}`);
  const schema = tsj.createGenerator(config).createSchema(config.type);
  const schemaString = JSON.stringify(schema, null, 2);
  fs.writeFile(output_path, schemaString, (err) => {
    if (err) throw err;
  });
}
function readFolder(fold) {
  if (fs.statSync(fold).isFile()) {
    generateSchema(fold, path.basename(fold));
    return;
  }
  const files = fs.readdirSync(fold);
  files.forEach((file) => {
    const fullPath = path.join(fold, file);
    const stat = fs.statSync(fullPath);
    if (stat.isFile()) {
      generateSchema(fullPath, file);
    } else {
      readFolder(fullPath);
    }
  });
}

readFolder("./test.ts");
