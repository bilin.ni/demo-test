import React,{useState} from "react";

function App() {
  const [count] = useState(0);
  return (
    <div>
       <h1>SubApplication React version-{React.version}</h1>
      <h1>Typescript</h1>
      <h2>App 1</h2>
      <h4>hook test {count}</h4>
    </div>
  );
}

export default App;
