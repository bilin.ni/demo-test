const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const path = require('path');
const federationConfig = {
  name: 'app',
  filename: 'remoteEntry.js',
  	/**
	 * Type of library (types included by default are 'var', 'module', 'assign', 'assign-properties', 'this', 'window', 'self', 'global', 'commonjs', 'commonjs2', 'commonjs-module', 'commonjs-static', 'amd', 'amd-require', 'umd', 'umd2', 'jsonp', 'system', but others might be added by plugins).
	 */
  
  // library: { type: 'module' },
  library: { type: 'var', name: 'app'},
  // exposes: {
  //   './App': './src/App',
  //   './newReact': require('react'),
  //   './newReactDOM': require('react-dom'),
  // },
  exposes: {
    './inject': './src/inject',
  },

};
module.exports = {
  entry: './src/index',
  mode: 'development',
  // target: 'es2020',
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    port: 3000,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
    },
  },
  output: {
    publicPath: 'auto'
  },
  // experiments: {
  //   outputModule: true,
  // },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-react', '@babel/preset-typescript'],
        },
      }
    ],
  },
  plugins: [
    new ModuleFederationPlugin(federationConfig),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
