import { defineConfig } from 'umi';

const deps = require('./package.json').dependencies;
console.log(deps.react, deps['react-dom']);
process.env.WEBPACK_FS_CACHE = 'none';
export default defineConfig({
  webpack5: {},
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [{ path: '/', component: '@/pages/index' }],
  fastRefresh: {},
  chainWebpack: (config: any, { webpack }) => {
    config.output.publicPath('auto');
    const { ModuleFederationPlugin } = webpack.container;
    console.log('===============>');
    console.log(webpack.version);
    console.log('===============>');
    config.plugin('mf').use(ModuleFederationPlugin, [
      {
        name: 'mf1',
        filename: 'remoteEntry.js',
        library: { type: 'var', name: 'mf1'},
        // library: { type: 'system' },
        // library: { type: 'module' },
        exposes: {
          './NoData': './src/pages/noData.tsx',
        },
        // shared: {
        //   react: {
        //     requiredVersion: deps.react,
        //     // singleton: true,
        //   },
        //   'react-dom': {
        //     requiredVersion: deps['react-dom'],
        //     // singleton: true,
        //   },
        // },
      },
    ]);
  },
});
