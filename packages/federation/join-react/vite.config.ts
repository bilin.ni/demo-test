import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import federation from "@originjs/vite-plugin-federation";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    federation({
      name: "container",
      filename: "remoteEntry.js",
      library: { type: 'var', name: 'container' },
      remotes: {
        'mf1': {
          external: "http://localhost:8001/remoteEntry.js",
          format: "var",
        },
        'app': {
          external: 'http://localhost:3000/remoteEntry.js',
          format: "var",
        }
      },
      // shared: {
      //   ...deps,
      //   'react-dom': {
      //     import: 'react-dom', // the "react" package will be used a provided and fallback module
      //     shareKey: 'react-dom', // under this name the shared module will be placed in the share scope
      //     shareScope: 'legacy', // share scope with this name will be used
      //     singleton: true, // only a single version of the shared module is allowed
      //   },
      // }
    }),
  ],
});
