/// <reference types="vite/client" />
declare module "mf1/NoData" {
	const NoData: React.ComponentType;

	export default NoData;
}
declare module "app/App" {
	const TestApp: React.ComponentType;

	export default TestApp;
}