// import { useState } from 'react'
import React, {Suspense, useEffect, useState} from 'react';
import './App.css';
const NoData = React.lazy(()=>import('mf1/NoData'));
import { inject, cleanup } from 'app/inject';
// import {
//   inject, cleanup
// } from 'mf1/inject';

const parentElementId = 'parent';
function App() {
  const [count, setCount] = useState(0)
  useEffect(() => {
    inject(parentElementId);
    return ()=> cleanup();
  }, []);
  return (
    <div className="App">
       <h1>Host Application - React Version {React.version}</h1>
       <Suspense fallback={<div>remote loaded fail</div>}>
          <NoData></NoData>
       </Suspense>
       <div style={{
        border: '2px solid red'
       }}>
        <h2>Sub Application</h2>
          <div id={parentElementId}></div>
       </div>
       
  
    </div>
  )
}

export default App
