import { renderWithQiankun, qiankunWindow } from 'vite-plugin-qiankun/dist/helper';
import React from "react";
import ReactDOM from "react-dom";
import { createRoot } from 'react-dom/client';


import { App } from "./App";

function render(props: any) {
  const { container } = props;
  const root = createRoot(container
    ? container.querySelector("#root")
    : document.getElementById("root") as HTMLElement);
root.render(<App />);
}
renderWithQiankun({
  mount(props) {
    console.log('mount');
    render(props);
  },
  bootstrap() {
    console.log('bootstrap');
  },
  unmount(props: any) {
    console.log('unmount');
    const { container } = props;
    const mountRoot = container?.querySelector('#root');
    ReactDOM.unmountComponentAtNode(
      mountRoot || document.querySelector('#root')
    );
  },
  update: function (props: any): void | Promise<void> {
    console.log(props);
  }
});

if (!qiankunWindow.__POWERED_BY_QIANKUN__) {
  render({});
}
