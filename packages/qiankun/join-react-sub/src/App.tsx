import { useState } from 'react'
import './App.css'

export function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <h1>Join react sub--vite</h1>
      <h2>test hook {count}</h2>
    </div>
  )
}
