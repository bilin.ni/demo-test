import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import reactRefresh from '@vitejs/plugin-react-refresh'
import qiankun from 'vite-plugin-qiankun';

const useDevMode = true     // 如果是在主应用中加载子应用vite,必须打开这个,否则vite加载不成功, 单独运行没影响
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [ react(), qiankun('join-react-sub', {useDevMode })],
  server: {
    host: '0.0.0.0' ,// 暴露内网ip
    port: 7316,
    cors: true,
  }
})
