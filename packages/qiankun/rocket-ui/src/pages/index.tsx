import styles from './index.less';
import { useState } from 'react';
// import { MicroApp } from 'umi';
export default function IndexPage() {
  const [count] = useState(1);
  return (
    <div>
      <h1>Umi Subapplication</h1>
      <h1 className={styles.title}>Page index{count}</h1>
      {/* <MicroApp name="join-react-sub" /> */}
    </div>
  );
}
