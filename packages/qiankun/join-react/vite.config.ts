import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
const currentDomain = 'joinhorizons.com';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      // '/api': {
      //     target: `http://localhost:8008`,
      //     changeOrigin: true,
      //     pathRewrite: {
      //         '^/api': '',
      //     },
      // },
      '/api': {
          target: `https://integration-api.${currentDomain}`,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
      },
      '/media': {
          target: `https://integration.${currentDomain}`,
          changeOrigin: true,
      },
      // oss proxy
      '/integration_docs': {
          target: 'https://rocketg-dev.oss-cn-shanghai.aliyuncs.com',
          changeOrigin: true,
      },
      '/staging_docs': {
          target: 'https://rocketg-dev.oss-cn-shanghai.aliyuncs.com',
          changeOrigin: true,
      },
  },
  }
})
