import React from 'react'
import ReactDOM from "react-dom";
import App from './App'
import './index.css'
import {
  registerMicroApps,
  runAfterFirstMounted,
  start,
} from 'qiankun';
import { createRoot } from 'react-dom/client';

const apps  = [{
  "name": "sub-app",
  "type": "react",
  "devEntry": "http://localhost:8001",
  "entry": "http://localhost:8001",
  "container": "#root",
  "activeRule": "/sub-app"
},{
  "name": "qiankun-app",
  "type": "react",
  "devEntry": "http://localhost:3001",
  "entry": "http://localhost:3001",
  "container": "#root",
  "activeRule": "/qiankun-app"
},{
  "name": "join-react-sub",
  "type": "react",
  "devEntry": "http://localhost:7316",
  "entry": "http://localhost:7316",
  "container": "#root",
  "activeRule": "/join-react-sub"
},{
  "name": "joinv1",
  "type": "react",
  "devEntry": "http://localhost:8000",
  "entry": "http://localhost:8000",
  "container": "#root",
  "activeRule": "/joinv1"
}];
// window.__MICRO_APP__NAME__ = 'main';
// window.__MICRO_APPS__ = apps;
async function main() {
  const root = createRoot(document.getElementById('root') as HTMLElement);
  root.render(<App />);
}

function initMicroApps() {
  // registry micro apps
  registerMicroApps(apps, {
    beforeLoad: (app: any) => {
      console.log("before load", app.name);
      return Promise.resolve();
    },
    beforeMount: (app: any) => {
      console.log("before mount", app.name);
      return Promise.resolve();
    },
    afterMount: (app: any) => {
      console.log("after mount", app.name);
      return Promise.resolve();
    }
  });

  start();

  runAfterFirstMounted(() => {
    console.log('[MainApp] first app mounted');
  });
}
// init main application first
main().then(() => {
  initMicroApps();
})