import './App.css'
import { i18n } from '@/i18n';
import React,{useState} from 'react';

function App() {
  console.log("what")
  const [count] = useState(1)
  return (
    <div className="App">
      <div>{i18n.t('hello')}</div>
      <div>国家{count}</div>
    </div>
  )
}

export default App
