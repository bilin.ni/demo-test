// translationRunner.js
import translations from 'react-intl-translations-manager';

const manageTranslations = translations.default;
// es2015 import
// import manageTranslations from 'react-intl-translations-manager';

manageTranslations({
  messagesDirectory: 'src/**',
  translationsDirectory: 'src/i18n/locales/',
  singleMessagesFile: true,
  languages: ['en'] // any language you need
});