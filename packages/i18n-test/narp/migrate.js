import fs from 'fs';
function flattenObjectToKeyArray(ob) {
    let toReturn = [];
    for (let prop in ob) {
      if (!ob.hasOwnProperty(prop)) continue;
  
      if ((typeof ob[prop]) == 'object' && ob[prop] !== null) {
        let flatObject = flattenObjectToKeyArray(ob[prop]);
        for (let idx = 0; idx < flatObject.length; idx++) {
          toReturn.push(prop + '.' + flatObject[idx]);
        }
      } else {
        toReturn.push(prop);
      }
    }
    return toReturn;
}
function get(obj,keys=[]) {
    if(keys.length === 1) {
        return obj[keys[0]];
    }
    let key = keys.shift();
    return get(obj[key],keys);
}
const targetFile = '/Users/nibilin/Codes/join-react-alpha/apps/web/src/i18n/en.json';
const language = fs.readFileSync(targetFile).toString();
const obj = JSON.parse(language);
const flatKeys = flattenObjectToKeyArray(obj);
let output = [];
flatKeys.forEach((key)=>{
    output.push({
        "term": key,
        "definition": get(obj,key.split('.'))
    })
    // output[key] = {
    //     "msgid": key,
    //     "msgstr": get(obj,key.split('.'))
    // }
});
fs.writeFileSync('./output.json',JSON.stringify(output));
