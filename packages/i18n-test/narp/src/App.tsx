import { useState } from 'react'
import './App.css'
import {i18n,i18nConfig} from './i18n/index';
function App() {
  const [count] = useState(0)
  return (
    <div className="App">
      <h1>count state:{count}</h1>
      <button onClick={()=>{
        i18nConfig.locale = 'en';
      }}>English</button>
      <button  onClick={()=>{
        i18nConfig.locale = 'zh';
      }}>中文</button>
      <div>{i18n('hello world')}</div>
      <div>{i18n('hello world 2')}</div>
    </div>
  )
}

export default App
