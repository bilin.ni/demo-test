module.exports = {
  entry: ['src'],
  exclude: [],
  output: ['src'],
  disableAutoTranslate: true,
  extractOnly: false,
  translator: null,
  ignoreComponents: [],
  ignoreMethods: [],
  primaryLocale: 'en-US',
  supportedLocales: ['zh-CN', 'en-US'],
  importCode: "import { i18n } from '@/i18n/index';",
  i18nObject: 'i18n',
  i18nMethod: 't',
  prettier: { singleQuote: true, trailingComma: 'es5', endOfLine: 'lf' },
  localeConf: { type: 'file', folder: 'locales' },
};
