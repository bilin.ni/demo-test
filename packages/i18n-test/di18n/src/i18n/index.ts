import { I18n } from 'i18n-js';
import en from './locales/en.json';
export const i18n = new I18n({ en });
i18n.missingTranslation.register(
    "missingTranslation",
    (i18n, scope) => {
        return (scope ? scope : 'missing translation') as string;
    }
)
i18n.missingBehavior = 'missingTranslation';
const defaultLocale = 'en';
i18n.defaultLocale = defaultLocale;
i18n.locale = defaultLocale;

